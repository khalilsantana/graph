module Graph {
    requires java.desktop;
    requires javafx.base;
    requires javafx.fxml;
    requires javafx.controls;
    requires guru.nidi.graphviz;
    opens br.univali.graph.gui to javafx.graphics, javafx.fxml, javafx.controls;
    exports br.univali.graph.gui;
}
package br.univali.graph.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/main.fxml"));
        Parent root = fxmlLoader.load();
//        root.getStylesheets().add("/main.css");
        primaryStage.setTitle("Graph");
//        primaryStage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/icons/pluto.png"))));
        primaryStage.setScene(new Scene(root));
        Controller controller = fxmlLoader.getController();
        controller.setStage(primaryStage);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }
}
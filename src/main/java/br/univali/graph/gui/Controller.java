package br.univali.graph.gui;

import br.univali.graph.model.Graph;
import br.univali.graph.model.GraphLibrary;
import br.univali.graph.model.Vertex;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;

public class Controller {
    public Button vInsert;
    public TextArea consoleTextArea;
    public TextField vertexNickname;
    public TextField vertexXPos;
    public TextField vertexYPos;
    public ComboBox<Vertex> vertexLinkComboBoxStart;
    public ComboBox<Vertex> vertexLinkComboBoxEnd;
    public TextField linkCostTextField;
    public ImageView popImg;
    @FXML
    private ImageView graphImage;
    @FXML
    private Stage stage;

    private Graph g = GraphLibrary.m2_ex4();

    public void renderGraph() {
        try {
            var str = g.graph2dot();
            Graph.saveToDisk("/tmp/graph.png", Graph.dot2viz(str).get());
            Image image = new Image(new FileInputStream("/tmp/graph.png"));
            ImageView imageView = new ImageView(image);
            imageView.fitHeightProperty();
            imageView.fitWidthProperty();
            imageView.preserveRatioProperty();
            graphImage.setImage(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateVertexSelectionList();
    }

    public void setStage(Stage primaryStage) {
        this.stage = primaryStage;
        renderGraph();
    }

    public void createVertex() {
        try {
            if (!vertexNickname.getText().isEmpty() &&
                    !vertexXPos.getText().isEmpty() &&
                    !vertexYPos.getText().isEmpty()) {
                var xPos = Integer.parseInt(vertexXPos.getText());
                var yPos = Integer.parseInt(vertexYPos.getText());
                g.insert(vertexNickname.getText(), xPos, yPos);
            } else {
                if (vertexNickname.getText().isEmpty()) {
                    g.insert();
                } else {
                    g.insert(vertexNickname.getText());
                }
            }
        } catch (NumberFormatException e) {
            consoleTextArea.appendText("Failed converting values to floats!");
            consoleTextArea.appendText(e.getMessage());
        }
        renderGraph();
    }

    public void updateVertexSelectionList() {
        vertexLinkComboBoxStart.setItems(FXCollections.observableList(g.vertices));
        vertexLinkComboBoxEnd.setItems(FXCollections.observableList(g.vertices));
    }

    public boolean link() {
        int cost = 1;
        try {
            if (!linkCostTextField.getText().isEmpty()) {
                cost = Integer.parseInt(linkCostTextField.getText());
            }
        } catch (NumberFormatException e) {
            consoleTextArea.appendText("\nFailed converting link cost!\n");
            consoleTextArea.appendText(e.getMessage());
            return false;
        }
        Vertex start, end;
        try {
            start = vertexLinkComboBoxStart.getValue();
            end = vertexLinkComboBoxEnd.getValue();
        } catch (Exception e) {
            consoleTextArea.appendText("\nEither Start or End is not selected, link operation canceled");
            return false;
        }
        var result = g.link(start.getId(), end.getId(), cost);
        renderGraph();
        return result;
    }

    public boolean unlink() {
        Vertex start, end;
        try {
            start = vertexLinkComboBoxStart.getValue();
            end = vertexLinkComboBoxEnd.getValue();
        } catch (Exception e) {
            consoleTextArea.appendText("\nEither Start or End is not selected, unlink operation canceled");
            return false;
        }
        var result = g.unlink(start.getId(), end.getId());
        renderGraph();
        return result;
    }

    public void aStar() {
        Vertex start, end;
        try {
            start = vertexLinkComboBoxStart.getValue();
            end = vertexLinkComboBoxEnd.getValue();
        } catch (Exception e) {
            consoleTextArea.appendText("\n" + e.getMessage());
            return;
        }
        g.aStar(start.getId(), end.getId());
        renderGraph();
    }

    public void bfs() {
        Vertex start, end;
        try {
            start = vertexLinkComboBoxStart.getValue();
            end = vertexLinkComboBoxEnd.getValue();
        } catch (Exception e) {
            consoleTextArea.appendText("\n" + e.getMessage());
            return;
        }
        g.bfs(start.getId(), end.getId());
        renderGraph();
    }

    public void dfs(ActionEvent actionEvent) {
        Vertex start, end;
        try {
            start = vertexLinkComboBoxStart.getValue();
            end = vertexLinkComboBoxEnd.getValue();
        } catch (Exception e) {
            consoleTextArea.appendText("\n" + e.getMessage());
            return;
        }
        g.dfs(start.getId(), end.getId());
        renderGraph();
    }

    public void isPlanar() {
        var result = g.isPlannar();
        consoleTextArea.appendText("\nIs plannar: " + result);
    }

    public void welshPowell() {
        var result = g.welshPowell();
        consoleTextArea.appendText(result);
        renderGraph();
    }
}

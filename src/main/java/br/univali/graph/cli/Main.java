package br.univali.graph.cli;

import br.univali.graph.model.Graph;
import br.univali.graph.model.GraphLibrary;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        var g = GraphLibrary.m2_ex4();
        g.aStar(8, 12);
        //System.out.println(g);
        var dot = g.graph2dot();
        try {
            Graph.saveToDisk("/home/khalil/graph.png", Graph.dot2viz(dot).orElseThrow());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(dot);
    }
}

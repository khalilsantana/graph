package br.univali.graph.cli;

import br.univali.graph.model.Graph;

public class ColorTests {
    public static void main(String[] args) {
        Graph g = new Graph.GraphBuilder().get();
        // Slide 30 de coloração
        g.insertMany(5);
        // Conexões de A - 0
        g.link(0, 1);
        g.link(0, 2);
        g.link(0, 3);
        g.link(1, 4);
        //g.link(1, 2);
        //g.link(2,3);
        g.link(2, 4);
        g.link(3, 4);
        //g.link(3,0);
        // Conexões de F - 5
        g.welshPowell();
        System.out.println(g.graph2dot());
        System.out.println(g.dot2viz(g.graph2dot()).isPresent());
    }
}

package br.univali.graph.cli;

import br.univali.graph.model.Graph;
import br.univali.graph.model.GraphLibrary;

import java.io.IOException;

public class PCVTest {
    public static void main(String[] args) {
        Graph g = GraphLibrary.m3();
        g.elitism(20, 100);
        var img = Graph.dot2viz(g.graph2dot()).get();
        try {
            Graph.saveToDisk("/home/khalil/graph.png", img);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

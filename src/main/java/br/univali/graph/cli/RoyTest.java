package br.univali.graph.cli;

import br.univali.graph.model.Graph;

public class RoyTest {
    public static void main(String[] args) {
        Graph g = new Graph.GraphBuilder().isDirected(true).allowDupeArcs(true).get();
        g.insertMany(3);
        g.link(0, 1);
        g.link(1, 2);
        g.link(2, 0);
        System.out.println(g);
        System.out.println("Matriz de Adjacencia:\n" + g.adj());
        System.out.println(g.incidencia());
        System.out.println(g.graph2dot());
        allConnected(g);
        System.out.println("\n-------\n");
        twoGroups(g);
    }

    public static void allConnected(Graph g) {
        System.out.println("[Roy - All connected]");
        g.roy();
    }

    public static void twoGroups(Graph g) {
        System.out.println("[Roy - Two Groups]");
        // Delete ARC_ID=2 (V1->V2)
        g.unlink(2);
        g.link(2, 1);
        System.out.println(g.graph2dot());
        g.roy();
    }
}

package br.univali.graph.cli;

import br.univali.graph.model.Graph;
import br.univali.graph.model.Vertex;

import java.util.Optional;

public class SearchAlgorithms {
    public static void main(String[] args) {
        System.out.println("[BFS]");
        Graph g = new Graph.GraphBuilder().allowDupeArcs(true).get();
        g.insertMany(7);
        g.link(0, 1);
        g.link(0, 2);
        g.link(1, 2);
        g.link(2, 5);
        g.link(2, 6);
        g.link(2, 4);
        g.link(2, 3);
        g.link(4, 3);
        System.out.println(g);
        System.out.println(g.adj());
        System.out.println(g.incidencia());
        //System.out.println(g.graph2json());
        System.out.println(g.graph2dot());
        //System.out.println(g.prim(0));
        System.out.println(bfsTest(g, 0, 5));
        System.out.println(dfsTest(g, 0, 5));
    }

    public static Optional<Vertex> bfsTest(Graph g, int v_id, int t_id) {
        return g.bfs(v_id, t_id);
    }

    public static Optional<Vertex> dfsTest(Graph g, int v_id, int t_id) {
        return g.dfs(v_id, t_id);
    }
}

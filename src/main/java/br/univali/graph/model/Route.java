package br.univali.graph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Route implements Comparable {
    private final Double mutationRate = 0.01d;
    public List<Vertex> route = new ArrayList<>();
    public Integer fitness;
    private Random rnd;

    public Route(Long seed) {
        rnd = new Random(seed);
    }

    public void addStop(Vertex v) {
        route.add(v);
    }

    @Override
    public int compareTo(Object o) {
        return this.fitness.compareTo(((Route) o).fitness);
    }

    public int calcFitness() {
        var distance = 0;
        for (int i = 0; i < route.size() - 1; i++) {
            distance += distance(route.get(i), route.get(i + 1));
        }
        distance += distance(route.get(route.size() - 1), route.get(0));
        this.fitness = distance;
        return this.fitness;
    }

    public int distance(Vertex vs, Vertex ve) {
        if (vs == null) {
            return 1000;
        }
        var hasArc = vs.arcs.stream().filter(a -> a.end == ve).findFirst();
        var dst = hasArc.map(arc -> arc.cost).orElse(1000);
        //System.out.println("Distance between " + vs + " and " + ve + " is " + dst + "KM");
        return dst;
    }

    public void mutate() {
        var random = rnd.nextDouble();
        if (random <= mutationRate) {
//            System.out.print("Mutation happened! R=" + random);
            var v_a = rnd.nextInt(0, route.size());
            var v_b = v_a;
            do {
                v_b = rnd.nextInt(0, route.size());
            } while (v_b == v_a);
//            System.out.println(" Swapped " + route.get(v_a) + " and " + route.get(v_b));
            Collections.swap(route, v_a, v_b);
            fitness = calcFitness();
        }
    }

    protected Route deepCopy() {
        var r = new Route(rnd.nextLong());
        for (Vertex v : this.route) {
            r.addStop(v);
        }
        return r;
    }

    public ChildRoute pmx(Route other) {
        var cut_start = 3;
        var cut_end = cut_start + 3;
//        System.err.println("P1: " + this);
//        System.err.println("P2: " + other);
        Route firstChild = deepCopy();
        Route secndChild = deepCopy();
        for (int p = cut_start; p < cut_end; p++) {
            var vo = other.route.get(p);
            var vt = this.route.get(p);
//            System.err.println("Gravar " + vt.nickname + " em idx: " + p);
            Collections.swap(firstChild.route,
                    p,
                    this.route.indexOf(vo)
            );
            Collections.swap(secndChild.route,
                    p,
                    other.route.indexOf(vt)
            );
//            firstChild.route.set(p, vo);
//            secndChild.route.set(p, vt);
        }
        debugPMX(firstChild);
        debugPMX(secndChild);
        return new ChildRoute(firstChild, secndChild);
    }

    private void debugPMX(Route r) {
        if (r.route.stream().distinct().toList().size() < route.size()) {
            throw new IllegalStateException("Invalid Route Generated!\n" + "C_: " + r);
        }
//        System.out.println(r);
    }

    private Route nullRouteInit() {
        var allNulls = new Route(rnd.nextLong());
        for (int i = 0; i < route.size(); i++) {
            allNulls.route.add(null);
        }
        return allNulls;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Route: fit: " + calcFitness());
        sb.append("    [");
        for (Vertex v : route) {
            if (v != null) {
                sb.append(v.toNickname()).append(", ");
            } else {
                sb.append("_").append(", ");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.setCharAt(sb.length() - 1, ']');
        sb.append("\n");
        return sb.toString();
    }

    protected static class ChildRoute {
        public final Route firstChild;
        public final Route secndChild;

        public ChildRoute(Route firstChild, Route secndChild) {
            this.firstChild = firstChild;
            this.secndChild = secndChild;
            firstChild.calcFitness();
            secndChild.calcFitness();
        }
    }
}

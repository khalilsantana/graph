package br.univali.graph.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Vertex {
    private static int ID = 0;
    protected final int id;
    protected LinkedList<Arc> arcs = new LinkedList<>();
    protected String nickname = null;
    protected Integer xPos;
    protected Integer yPos;

    protected double cost, heuristic, function;
    protected Vertex parent;
    protected Integer color = 0;

    public Vertex() {
        this.id = ID++;
    }

    public Vertex(String nickname, Integer xPos, Integer yPos) {
        this.id = ID++;
        this.nickname = nickname;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public Vertex(String nickname) {
        this.id = ID++;
        this.nickname = nickname;
    }

    public static Optional<Vertex> findById(List<Vertex> list, int v_end) {
        return list.stream().filter(v -> v.id == v_end).findFirst();
    }

    public static Optional<Vertex> findByNickname(List<Vertex> list, String nick) {
        return list.stream().filter(v -> v.nickname.equals(nick)).findFirst();
    }

    public static LinkedList<Vertex> getAncestors(List<Vertex> list, Vertex v) {
        var ancestors = new LinkedList<Vertex>();
        for (Vertex w : list) {
            var count = w.arcs.stream().filter(a -> a.end.id == v.id).count();
            // If any arc from list points directly to v
            if (count != 0) {
                ancestors.add(w);
            }
        }
        return ancestors;
    }

    static Integer manhattanDistance(Vertex a, Vertex b) {
        return Math.abs(a.xPos - b.xPos) + Math.abs(a.yPos - b.yPos);
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vertex o) {
            return o.id == this.id;
        }
        return false;
    }

    public String toDebugString() {
        return "V" + id + "_C" + color +
                "(" + xPos + "," + yPos + ")" +
                "{ a=" + arcs +
                " }";
    }

    public int compareArcCountTo(Vertex other) {
        Integer count = this.arcs.size();
        Integer countOther = other.arcs.size();
        return count.compareTo(countOther);
    }

    public String toString() {
        var out = "V" + id;
        if (nickname != null) {
            out += "_" + nickname;
        }
        return out;
    }

    public String toShortStringColor() {
        var out = "V" + id;
        if (nickname != null) {
            out += "_" + nickname;
        }
        out += "[fillcolor = ";
        if (color == 0) {
            out += "white";
        } else {
            out += color;
        }
        return out + "]";
    }

    public List<Vertex> getSuccessors() {
        return arcs.stream().map(a -> a.end).toList();
    }

    boolean isAdjacent(Vertex other) {
        return arcs.stream().map(a -> a.end).anyMatch(v -> v == other) ||
                other.arcs.stream().map(a -> a.end).anyMatch(v -> v == this);
    }

    public String toNickname() {
        return this.nickname;
    }

    public Vertex deepCopy() {
        var v = new Vertex(nickname, xPos, yPos);
        v.arcs.addAll(this.arcs);
        return v;
    }
}

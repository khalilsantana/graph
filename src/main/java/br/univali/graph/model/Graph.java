package br.univali.graph.model;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Graph {
    private final boolean isDirected;
    private final boolean allowDupeArcs;
    private final boolean calcArcLenghtOnLink;
    private final double offspringRate = 0.6d;
    public LinkedList<Vertex> vertices = new LinkedList<>();
    protected double scale = 2;
    private Random rnd = new Random(54);

    public Graph(GraphBuilder gb) {
        this.isDirected = gb.isDirected;
        this.allowDupeArcs = gb.allowDupeArcs;
        this.calcArcLenghtOnLink = gb.calcArcLengthOnLink;
    }

    public static Optional<BufferedImage> dot2viz(String dot) {
        MutableGraph vizGraph;
        try {
            vizGraph = new Parser().read(dot);
        } catch (IOException e) {
            return Optional.empty();
        }
        BufferedImage img = Graphviz.fromGraph(vizGraph).width(1080).render(Format.PNG).toImage();
        return Optional.of(img);
    }

    public static void saveToDisk(String path, BufferedImage img) throws IOException {
        File f = new File(path);
        ImageIO.write(img, "png", f);
    }

    public void insert() {
        vertices.add(new Vertex());
    }

    public void insertMany(int howMany) {
        for (int i = 0; i < howMany; i++) {
            vertices.add(new Vertex());
        }
    }

    public void insert(String nickname, Integer xPos, Integer yPos) {
        vertices.add(new Vertex(nickname, xPos, yPos));
    }

    public void insert(String nickname) {
        vertices.add(new Vertex(nickname));
    }

    public boolean link(String v_start, String v_end, int cost) {
        var start = Vertex.findByNickname(vertices, v_start).orElseThrow();
        var end = Vertex.findByNickname(vertices, v_end).orElseThrow();
        return link(start.id, end.id, cost);
    }

    // QoL method with a hardcoded 0 cost
    public boolean link(int v_start, int v_end) {
        return this.link(v_start, v_end, 1);
    }

    public boolean link(int v_start, int v_end, int cost) {
        var start = Vertex.findById(vertices, v_start).orElseThrow();
        var end = Vertex.findById(vertices, v_end).orElseThrow();
        var already_exists = start.arcs.stream().anyMatch(a -> a.end == end);
        if (!already_exists || allowDupeArcs) {
            if (calcArcLenghtOnLink) {
                cost = Vertex.manhattanDistance(start, end);
            }
            var arc = new Arc(end, cost);
            start.arcs.add(arc);
            if (!isDirected) {
                end.arcs.add(new Arc(arc.id, start, cost));
            }
            return true;
        }

        return false;
    }

    public boolean unlink(int v_start, int v_end) {
        var start = Vertex.findById(vertices, v_start).orElseThrow();
        var arc = start.arcs.stream().filter(a -> a.end.id == v_end).findFirst();
        if (arc.isPresent()) {
            start.arcs.remove(arc.get());
            if (!isDirected) {
                Vertex.findById(vertices, v_end).orElseThrow().arcs.removeIf(a -> a.end == start);
            }
            return true;
        }
        return false;
    }

    public void unlink(int arc_id) {
        // Delete arc by id
        vertices.forEach(v -> v.arcs.removeIf(arc -> arc.id == arc_id));
    }

    public boolean delete(int v_id) {
        // Delete vertex by id
        var output = vertices.removeIf(v -> v.id == v_id);
        // Remove arcs that point to this vertex
        vertices.forEach(v -> v.arcs.removeIf(a -> a.end.id == v_id));
        return output;
    }

    public int isAdjacent(int v_start, int v_end) {
        var start = Vertex.findById(vertices, v_start).orElseThrow();
        var positive = start.arcs.stream().anyMatch(a -> a.end.id == v_end);
        var output = 0;
        if (positive) {
            output = 1;
        }
        if (isDirected) {
            var end = Vertex.findById(vertices, v_end).orElseThrow();
            var negative = end.arcs.stream().anyMatch(a -> a.end.id == v_start);
            if (negative) {
                output = -1;
            }
        }
        return output;
    }

    public AdjacencyMatrix adj() {
        //var tags = vertices.stream().map(Vertex::toShortString).toList();
        List<String> tags = new ArrayList<>();
        var max = vertices.stream().max(Comparator.comparingInt(Vertex::getId)).orElseThrow().id;
        var adj = new AdjacencyMatrix(tags, tags);
        for (Vertex v : vertices) {
            tags.add(v.toString());
            ArrayList<Integer> row = new ArrayList<>();
            for (int i = 0; i <= max; i++) {
                try {
                    row.add(isAdjacent(v.id, i));
                } catch (Exception ignored) {
                }
            }
            adj.pushInOrder(row);
        }
        return adj;
    }

    public AdjacencyMatrix incidencia() {
        var allArcs = vertices.stream().flatMap(v -> v.arcs.stream()).distinct().sorted(Comparator.comparingInt(Arc::getId)).toList();
        var arcTags = allArcs.stream().map(Arc::toShortString).toList();
        var vertTags = vertices.stream().map(Vertex::toString).toList();
        var adj = new AdjacencyMatrix(arcTags, vertTags);
        for (Arc a : allArcs) {
            ArrayList<Integer> row = new ArrayList<>();
            for (Vertex v : vertices) {
                if (v.arcs.contains(a)) {
                    row.add(1);
                } else {
                    if (a.end == v) {
                        row.add(-1);
                    } else {
                        row.add(0);
                    }
                }
            }
            adj.pushInOrder(row);
        }
        return adj;
    }

    public String mst(int v_id) {
        if (this.isDirected) {
            throw new IllegalStateException("This method can only be called when using non-directed graphs");
        }
        StringBuilder mst = new StringBuilder("graph prim{\n");
        var mstCost = 0;
        var s = Vertex.findById(vertices, v_id).orElseThrow();
        StringBuilder sb = new StringBuilder("MST - PRIM Method: starting vert: " + s + "\n");
        var visited = new HashSet<>(Collections.singleton(s));
        while (visited.size() != vertices.size()) {
            sb.append("Visited: ").append(visited.stream().map(Vertex::toString).toList()).append("\n");
            var reachableArcs = visited.stream().flatMap(v -> v.arcs.stream()) // get all arcs from visited verts
                    .filter(a -> !visited.contains(a.end)) //filter out arcs that end in already visited verts
                    .sorted(Comparator.comparingInt(Arc::getCost)).toList(); // order the remainder by cost
            var a = reachableArcs.get(0);
            mstCost += a.cost;
            s = visited.stream().filter(v -> v.arcs.contains(a)).findFirst().orElseThrow();
            sb.append("Reachable Arcs: ").append(reachableArcs).append("\n");
            sb.append("Visiting ").append(a.end.toString()).append(" via ").append(a.toShortString()).append("\n");
            mst.append('"').append(s).append('"').append(" -- ");
            mst.append('"').append(a.end).append('"').append("[label=").append('"').append(a.cost).append("\"]\n");
            visited.add(a.end);
        }
        mst.append("}");
        System.out.println(mst);
        System.out.println(sb);
        return mst.toString();
    }

    public Optional<Vertex> bfs(int v_id, int t_id) {
        var v = Vertex.findById(vertices, v_id).orElseThrow();
        StringBuilder sb = new StringBuilder("\n======\nBFS - startingVertex=" + v).append("\n");
        StringBuilder bfs = new StringBuilder("@staruml\ngraph bfs {\n");
        var marked = new LinkedList<>(Collections.singleton(v));
        var q = new LinkedList<>(Collections.singleton(v));
        System.out.println(sb);
        while (!q.isEmpty()) {
            sb = new StringBuilder();
            sb.append("Q: ").append(q.stream().map(Vertex::toString).toList()).append("\n");
            var current_v = q.removeFirst();
            var arcs = current_v.arcs.stream().sorted(Comparator.comparingInt(Arc::getEndId)).toList();
            var ends = arcs.stream().map(a -> a.end).toList();
            sb.append("Successors of ").append(current_v).append(": ").append(
                    ends.stream().map(Vertex::toString).toList());
            sb.append("\n");
            for (Vertex w : ends) {
                if (w.id == t_id) {
                    System.out.println("Found Vertex!: " + w + " via " + current_v);
                    bfs.append('"').append(current_v).append("\" -- ").append('"').append(w).append('"').append("\n");
                    bfs.append("}\n@enduml");
                    System.out.println(bfs);
                    return Optional.of(w);
                }
                if (!marked.contains(w)) {
                    sb.append("explore(").append(current_v).append(",").append(w).append(")").append("\n");
                    bfs.append('"').append(current_v).append("\" -- ").append('"').append(w).append('"').append("\n");
                    q.addLast(w);
                    marked.push(w);
                } else {
                    sb.append("dot_line(").append(current_v).append(",").append(w).append(")").append("\n");
                }
            }
            System.out.println(sb);
        }
        bfs.append("}\n@enduml");
        System.out.println(bfs);
        return Optional.empty();
    }

    public Optional<Vertex> dfs(int v_id, int t_id) {
        var v = Vertex.findById(vertices, v_id).orElseThrow();
        System.out.println("\n======\nDFS - startingVertex: " + v + " trying to find V" + t_id);
        var marked = new Stack<Vertex>();
        marked.push(v);
        return dfsInternal(marked, v, t_id);
    }

    private Optional<Vertex> dfsInternal(Stack<Vertex> marked, Vertex v, int t_id) {
        var successors = v.arcs.stream().map(a -> a.end).toList();
        System.out.println("successors of " + v + ": " + successors.stream().map(Vertex::toString).toList());
        System.out.println("Marked:" + marked.stream().map(Vertex::toString).toList());
        Optional<Vertex> maybeFound = Optional.empty();
        for (Vertex w : successors) {
            if (w.id == t_id) {
                // We found the vert!
                System.out.println("Found vertex!: " + w + " via " + v);
                return Optional.of(w);
            }
            if (!marked.contains(w)) {
                System.out.println("explore(" + v + "," + w + ")");
                marked.push(w);
                maybeFound = dfsInternal(marked, w, t_id);
            } else {
                System.out.println("dot_line(" + v + "," + w + ")");
            }
        }
        return maybeFound;
    }

    public List<Set<Vertex>> roy() {
        if (!this.isDirected) {
            throw new IllegalStateException("Roy method can only be used on directed graphs.");
        }
        var S = new LinkedList<Set<Vertex>>();
        // Copy vertices LL to prevent it from being modified here.
        var H = new LinkedList<>(vertices);
        var plus = new LinkedList<Vertex>();
        var negative = new LinkedList<Vertex>();
        while (!H.isEmpty()) {
            // Pick any vertex v such that v belongs to H.
            var v = H.getFirst();
            // Mark v + and -
            plus.add(v);
            negative.add(v);
            System.out.println("Starting from: " + v.toString());
            // While it's possible to mark with plus
            // Flood-find all calcArcLengthOnLink
            var sucessors = v.arcs.stream().filter(a -> !plus.contains(a.end)).map(a -> a.end).toList();
            while (!sucessors.isEmpty()) {
                plus.addAll(sucessors);
                sucessors = sucessors.stream().flatMap(n -> n.arcs.stream()).filter(a -> !plus.contains(a.end)).map(a -> a.end).toList();
            }
            // Find all arcs that point to the current vert (v)
            var antecessors = Vertex.getAncestors(H, v);
            while (!antecessors.isEmpty()) {
                negative.addAll(antecessors);
                var tmp = new LinkedList<Vertex>();
                for (Vertex x : antecessors) {
                    tmp.addAll(Vertex.getAncestors(H, x).stream().filter(k -> !negative.contains(k)).toList());
                }
                antecessors = tmp;
            }
            var Sn = plus.stream().filter(negative::contains).collect(Collectors.toSet());
            S.push(Sn);
            System.out.println("Marked [+]: " + plus.stream().map(Vertex::toString).toList());
            System.out.println("Marked [-]: " + negative.stream().map(Vertex::toString).toList());
            System.out.println("Intersection: S" + S.size() + " " + Sn.stream().map(Vertex::toString).toList());
            plus.clear();
            negative.clear();
            H.removeIf(Sn::contains);
        }
        return S;
    }

    public boolean isPlannar() {
        var result = false;
        StringBuilder sb = new StringBuilder();
        if (this.vertices.size() < 3) {
            sb.append("All graphs with less than 2 vertices are plannar by nature\n");
            result = true;
        }
        var arcCount = vertices.stream().flatMap(v -> v.arcs.stream()).distinct().count();
        var firstEquationRhs = 3 * vertices.size() - 6;
        sb.append("Arc Count: ").append(arcCount).append("; Vertex Count: ").append(vertices.size()).append("\n");
        sb.append("E <= 3*V - 6\n");
        sb.append(arcCount).append(" <= 3*").append(vertices.size()).append(" - 6\n");
        sb.append(arcCount).append(" <= ").append(3 * vertices.size()).append(" - 6\n");
        sb.append(arcCount).append(" <= ").append(3 * vertices.size() - 6);
        if (arcCount <= firstEquationRhs) {
            sb.append("   TRUE!\n");
            result = true;
        } else {
            sb.append("    FALSE!\n");
        }
        var cycles = containsCyclesOf3();
        if (cycles.isEmpty()) {
            sb.append("However, this graph DOESN'T contain cycles of length 3, as such we check the second equation:\n");
            var secondEquationRhs = 2 * vertices.size() - 4;
            sb.append("E <= 3*V - 6\n");
            sb.append(arcCount).append(" <= 2*").append(vertices.size()).append(" - 4\n");
            sb.append(arcCount).append(" <= ").append(2 * vertices.size()).append(" - 4\n");
            sb.append(arcCount).append(" <= ").append(2 * vertices.size() - 4);
            if (arcCount > secondEquationRhs) {
                sb.append("    FALSE!\n");
                result = false;
            } else {
                sb.append("    TRUE!\n");
            }
        } else {
            sb.append("Graph contains cycles of 3: ").append(cycles.get()).append("\n");
        }
        System.out.println(sb);
        return result;
    }

    public Optional<List<Vertex>> containsCyclesOf3() {
        for (Vertex v : vertices) {
            var firstHop = v.arcs.stream().map(a -> a.end).toList();
            for (Vertex s : firstHop) {
                var secondHop = s.arcs.stream().map(a -> a.end).toList();
                for (Vertex t : secondHop) {
                    var thirdHop = t.arcs.stream().map(a -> a.end).toList();
                    if (thirdHop.contains(v)) {
                        return Optional.of(List.of(s, t, v));
                    }
                }
            }
        }
        return Optional.empty();
    }

    public String welshPowell() {
        StringBuilder sb = new StringBuilder();
        var sorted = new ArrayList<>(vertices.stream().sorted(Vertex::compareArcCountTo).toList());
        Collections.reverse(sorted);
        var color = 0;
        sb.append("Vertices ranked by decreasing arc counts: ");
        sb.append(sorted.stream().map(Vertex::toString).toList()).append("\n");
        for (Vertex v : sorted) {
            if (v.color != 0) {
                continue;
            }
            sb.append("Testando vertice ").append(v).append("\n");
            v.color = ++color;
            sb.append("Pintando ").append(v).append(" da cor ").append(v.color).append("\n");
            for (Vertex w : sorted) {
                if (v != w && w.color == 0) {
                    sb.append("O vertice ").append(w).append(" é adjacente a ").append(v).append(": ");
                    if (!v.isAdjacent(w)) {
                        sb.append("falso!\n");
                        if (w.arcs.stream().map(a -> a.end).anyMatch(e -> Objects.equals(e.color, v.color))) {
                            sb.append("Vertice vizinho da mesma cor detectado, pulando vertice ").append(v).append("\n");
                            continue;
                        }
                        w.color = v.color;
                        sb.append("Pintando ").append(w).append(" da cor ").append(w.color).append("\n");
                    } else {
                        sb.append("verdadeiro\n");
                    }
                }
            }
        }
        return sb.toString();
    }

    public void aStar(int vStart, int vEnd) {
        var start = Vertex.findById(vertices, vStart).orElseThrow();
        var end = Vertex.findById(vertices, vEnd).orElseThrow();
        calculateArcDistances();
        StringBuilder sb = new StringBuilder();
        sb.append("Trying to find a route between ").append(start.toNickname()).append(" and ").append(end.toNickname());
        sb.append(" which is about ").append(Vertex.manhattanDistance(start, end)).append("KM").append(" away\n");
        var arcCount = vertices.stream().flatMap(v -> v.arcs.stream()).distinct().count();
        sb.append("Arc Count: ").append(arcCount).append("; Vertex Count: ").append(vertices.size()).append("\n");
        for (Vertex v : vertices) {
            var list = v.arcs.stream().map(a -> a.end).map(Vertex::toNickname).toList();
            sb.append(v.toNickname()).append(" ").append(list).append("\n");
        }
        var openList = new LinkedList<Vertex>();
        start.cost = 0;
        start.function = 0;
        start.heuristic = Vertex.manhattanDistance(start, end);
        openList.add(start);
        var closedList = new HashSet<Vertex>();
        Vertex currentNode = null;
        while (!openList.isEmpty()) {
            currentNode = lowestFuncCost(openList);
            openList.remove(currentNode);
            if (currentNode == end) {
                sb.append("Found vertex!\n");
                break;
            }
            closedList.add(currentNode);
            var successors = currentNode.arcs.stream().map(a -> a.end).toList();
            for (Vertex w : successors) {
                if (closedList.contains(w)) {
                    continue;
                }
                var tmpScore = currentNode.cost + Vertex.manhattanDistance(currentNode, w);
                if (openList.contains(w)) {
                    if (tmpScore < w.cost) {
                        w.cost = tmpScore;
                        w.parent = currentNode;
                    }
                } else {
                    w.cost = tmpScore;
                    openList.add(w);
                    w.parent = currentNode;
                }
                w.heuristic = Vertex.manhattanDistance(w, end);
                w.function = w.cost + w.heuristic;
            }

        }
        sb.append("Shortest path between ")
                .append(start)
                .append(" and ")
                .append(end)
                .append(" is ")
                .append(currentNode.cost).append("KM")
                .append(" via: ");
        sb.append(getPath(currentNode).stream().map(Vertex::toString).toList());
        sb.append("\n");
        System.out.println(sb);
    }

    private Vertex lowestFuncCost(List<Vertex> openList) {
        Vertex lowest = openList.get(0);
        for (Vertex n : openList) {
            if (n.function < lowest.function) {
                lowest = n;
            }
        }
        return lowest;
    }

    private List<Vertex> getPath(Vertex v) {
        LinkedList<Vertex> path = new LinkedList<>();
        while (v.parent != null) {
            path.add(v);
            Vertex finalV = v;
            v.parent.arcs.stream().filter(a -> a.end == finalV).forEach(a -> a.color = 1);
            if (!isDirected) {
                v.arcs.stream().filter(a -> a.end == finalV.parent).forEach(a -> a.color = 1);
            }
            v = v.parent;
        }
        path.add(v);
        Collections.reverse(path);
        return path;
    }

    public void calculateArcDistances() {
        for (Vertex v : vertices) {
            for (Arc a : v.arcs) {
                a.cost = Vertex.manhattanDistance(v, a.end);
            }
        }
    }

    public List<Route> genInitialPopulation(int size) {
        var pop = new ArrayList<Route>();
        while (pop.size() < size) {
            for (int i = 0; i < vertices.size(); i++) {
                pop.add(genRndRoute());
            }
        }
        return pop;
    }

    public Route genRndRoute() {
        var route = new Route(rnd.nextLong());
        var ids = new ArrayList<Integer>();
        for (int i = 0; i < vertices.size(); i++) {
            ids.add(i);
        }
        Collections.shuffle(ids, rnd);
        for (int i = 0; i < ids.size(); i++) {
            route.addStop(vertices.get(ids.get(i)));
        }
        route.calcFitness();
        return route;
    }

    public void elitism(int genLimit, int popSizeMax) {
        var pop = genInitialPopulation(popSizeMax);
        for (int i = 0; i < genLimit; i++) {
            System.out.println("=== Generation " + i + " ===");
            var offspring = offspring(pop);
            offspring.forEach(Route::mutate);
            pop.addAll(offspring);
            Collections.sort(pop);
            pop = pop.subList(0, popSizeMax);
            System.out.println("Best: " + pop.get(0).fitness);
            System.out.println(pop.subList(0, 5));
//            System.out.println(pop);
        }
        var best = pop.get(0).route;
        for (int i = 0; i < best.size() - 1; i++) {
            int finalI = i;
            best.get(i).arcs.stream().filter(a -> a.end == best.get(finalI + 1)).forEach(a -> a.color = 1);
            best.get(i + 1).arcs.stream().filter(a -> a.end == best.get(finalI)).forEach(a -> a.color = 1);
        }
        best.get(0).arcs.stream().filter(a -> a.end == best.get(best.size() - 1)).forEach(a -> a.color = 1);
        best.get(best.size() - 1).arcs.stream().filter(a -> a.end == best.get(0)).forEach(a -> a.color = 1);
    }

    public List<Route> offspring(List<Route> pop) {
        var popNew = new ArrayList<Route>();
        for (Route r : pop) {
            if (rnd.nextDouble() <= offspringRate) {
                var other = pop.get(rnd.nextInt(0, pop.size()));
//                System.out.print("Creating ofspring from " + r.fitness + " and " + other.fitness);
                var children = r.pmx(other);
                popNew.add(children.firstChild);
                popNew.add(children.secndChild);
//                System.out.println(" resulted in: " +
//                        children.firstChild.fitness +
//                        " and " + children.secndChild.fitness);
            }
        }
        return popNew;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GRAPH: isDirected=").append(isDirected).append(", allowDupeArcs=").append(allowDupeArcs).append("\n");
        for (Vertex v : vertices) {
            sb.append("    ").append(v).append("\n");
        }
        return sb.toString();
    }

    public String graph2dot() {
        StringBuilder sb = new StringBuilder();
        if (isDirected) {
            sb.append("digraph mygraph {\n");
        } else {
            sb.append("graph mygraph {\n");
        }
        sb.append("node [colorscheme=set28 style=filled]\n");
        sb.append("scale=").append(scale).append("\n");
//        sb.append("layout=neato\n");
        var seenArcs = new LinkedList<Arc>();
        for (Vertex v : vertices) {
            sb.append(v.toShortStringColor()).append("\n");
            for (Arc a : v.arcs) {
                if (!isDirected && seenArcs.contains(a)) {
                    continue;
                }
                seenArcs.push(a);
                sb.append('"').append(v).append('"');
                if (isDirected) {
                    sb.append(" -> ");
                } else {
                    sb.append(" -- ");
                }
                sb.append('"').append(a.end.toString()).append('"')
                        .append("[label=").append('"').append(a.toShortString()).append("(").append(a.cost).append(")\" weight=").append(a.cost)
                        .append(" color=");
                if (a.color != 0) {
                    sb.append("\"#FF0000\"");
                } else {
                    sb.append("\"#000000\"");
                }
                sb.append("]").append("\n");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    public static class GraphBuilder {
        private boolean isDirected = false;
        private boolean allowDupeArcs = false;

        // Useful for graphs where each Vertex has an x and y position
        private boolean calcArcLengthOnLink = false;

        public GraphBuilder isDirected(boolean b) {
            this.isDirected = b;
            return this;
        }

        public GraphBuilder allowDupeArcs(boolean b) {
            this.allowDupeArcs = b;
            return this;
        }

        public GraphBuilder calcArcLengthOnLink(boolean b) {
            this.calcArcLengthOnLink = b;
            return this;
        }

        public Graph get() {
            return new Graph(this);
        }
    }
}

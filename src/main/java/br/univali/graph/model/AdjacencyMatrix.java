package br.univali.graph.model;

import java.util.ArrayList;
import java.util.List;

public class AdjacencyMatrix {
    protected final List<List<Integer>> matrix = new ArrayList<>();
    protected final List<String> verticalTags;
    protected final List<String> horizontalTags;

    public AdjacencyMatrix(List<String> verticalTags, List<String> horizontalTags) {
        this.verticalTags = verticalTags;
        this.horizontalTags = horizontalTags;
    }

    public void pushInOrder(List<Integer> list) {
        this.matrix.add(list);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.horizontalTags).append("\n");
        for (int i = 0; i < verticalTags.size(); i++) {
            sb.append(verticalTags.get(i)).append(matrix.get(i)).append("\n");
        }
        return sb.toString();
    }
}

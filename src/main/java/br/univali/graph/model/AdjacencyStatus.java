package br.univali.graph.model;

public enum AdjacencyStatus {
    NOT_ADJACENT,
    STARTING_VERTEX,
    DESTINATION_VERTEX,
    BOTH_DIRECTIONS
}

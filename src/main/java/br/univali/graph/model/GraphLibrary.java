package br.univali.graph.model;

import java.util.Random;

public class GraphLibrary {
    public static Graph k3_3() {
        Graph g = new Graph.GraphBuilder().get();
        g.insertMany(6);
        g.link(0, 1);
        g.link(0, 3);
        g.link(0, 5);
        g.link(2, 1);
        g.link(2, 3);
        g.link(2, 5);
        g.link(4, 1);
        g.link(4, 3);
        g.link(4, 5);
        return g;
    }

    public static Graph k5() {
        Graph g = new Graph.GraphBuilder().get();
        g.insertMany(5);
        // V0
        g.link(0, 1);
        g.link(0, 2);
        g.link(0, 3);
        g.link(0, 4);
        // V1
        g.link(1, 2);
        g.link(1, 3);
        g.link(1, 4);
        // V2
        g.link(2, 3);
        g.link(2, 4);
        // V3
        g.link(3, 4);
        return g;
    }

    public static Graph m2_ex4() {
        var g = new Graph.GraphBuilder().calcArcLengthOnLink(true).get();
        g.insert("A", 950, 231);
        g.insert("B", 607, 486);
        g.insert("C", 891, 762);
        g.insert("D", 456, 19);
        g.insert("E", 821, 445);
        g.insert("F", 615, 792);
        g.insert("G", 922, 738);
        g.insert("H", 176, 406);
        g.insert("I", 935, 917);
        g.insert("J", 410, 894);
        g.insert("K", 58, 353);
        g.insert("L", 813, 10);
        g.insert("M", 139, 203);
        g.insert("N", 199, 604);
        g.insert("O", 272, 199);
        g.insert("P", 15, 747);
        g.insert("Q", 445, 932);
        g.insert("R", 466, 419);
        g.insert("S", 846, 525);
        g.insert("T", 203, 672);
        // Links A
        g.link(0, 1);
        g.link(0, 4);
        g.link(0, 6);
        g.link(0, 11);
        g.link(0, 18);
        // Links B
        g.link(1, 4);
        g.link(1, 18);
        g.link(1, 17);
        g.link(1, 5);
        g.link(1, 9);
        g.link(1, 19);
        // Links C
        g.link(2, 6);
        g.link(2, 8);
        g.link(2, 5);
        // Links D
        g.link(3, 12);
        g.link(3, 17);
        g.link(3, 11);
        // Links F
        g.link(5, 18);
        g.link(5, 9);
        // Links G
        g.link(6, 8);
        g.link(6, 18);
        // Links H
        g.link(7, 14);
        g.link(7, 13);
        g.link(7, 15);
        // Links I
        g.link(8, 16);
        // Links J
        g.link(9, 19);
        g.link(9, 15);
        g.link(9, 16);
        // Links K
        g.link(10, 12);
        g.link(10, 15);
        // Link M
        g.link(12, 14);
        // Links N
        g.link(13, 15);
        g.link(13, 17);
        g.link(13, 19);
        // Links P
        g.link(15, 19);
        // Links R
        g.link(17, 14);
        g.link(17, 11);
        return g;
    }

    public static Graph primexample() {
        var g = new Graph.GraphBuilder().get();
        g.insertMany(5);
        g.link(0, 1, 9);
        g.link(0, 2, 5);
        g.link(0, 3, 2);
        g.link(1, 3, 6);
        g.link(1, 4, 5);
        g.link(2, 3, 4);
        g.link(2, 4, 5);
        g.link(3, 4, 3);
        return g;
    }

    public static Graph loop4() {
        var g = new Graph.GraphBuilder().get();
        g.insertMany(4);
        g.link(0, 1);
        g.link(1, 2);
        g.link(2, 3);
        g.link(3, 0);
        g.link(0, 2);
        return g;
    }

    public static Graph m3() {
        var g = new Graph.GraphBuilder().isDirected(false)
                .allowDupeArcs(false)
                .calcArcLengthOnLink(false).get();
        g.insert("K");
        g.insert("E");
        g.insert("H");
        g.insert("L");
        g.insert("C");
        g.insert("F");
        g.insert("N");
        g.insert("G");
        //K
        g.link("K", "E", 10);
        g.link("K", "H", 73);
        g.link("K", "C", 70);
        g.link("K", "N", 60);
        g.link("K", "G", 90);
        //G
        g.link("G", "E", 40);
        g.link("G", "H", 80);
        g.link("G", "F", 55);
        //F
        g.link("F", "N", 30);
        g.link("F", "C", 20);
        g.link("F", "L", 10);
        //N
        g.link("N", "C", 47);
        //C
        g.link("C", "E", 10);
        g.link("C", "H", 30);
        g.link("C", "L", 10);
        //L
        g.link("L", "E", 5);
        g.link("L", "H", 40);
        //H
        g.link("H", "E", 60);
        return g;
    }

    public static Graph pmxTest() {
        var g = new Graph.GraphBuilder()
                .isDirected(false)
                .allowDupeArcs(false)
                .calcArcLengthOnLink(false).get();
        for (int i = 1; i < 10; i++) {
            g.insert(String.valueOf(i));
        }
        var rnd = new Random(42);
        var p1 = new Route(rnd.nextLong());
        p1.addStop(Vertex.findByNickname(g.vertices, "1").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "2").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "3").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "4").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "5").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "6").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "7").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "8").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "9").orElseThrow());
        var p2 = new Route(rnd.nextLong());
        p2.addStop(Vertex.findByNickname(g.vertices, "4").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "3").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "2").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "1").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "8").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "5").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "6").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "7").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "9").orElseThrow());
        var offspring = p1.pmx(p2);
        System.out.println(offspring.firstChild);
        System.out.println(offspring.secndChild);
        return g;
    }

    public static Graph pmxTest2() {
        var g = new Graph.GraphBuilder()
                .isDirected(false)
                .allowDupeArcs(false)
                .calcArcLengthOnLink(false).get();
        for (int i = 1; i < 9; i++) {
            g.insert(String.valueOf(i));
        }
        var rnd = new Random(42);
        var p1 = new Route(rnd.nextLong());
        //P1: Route: fit: 2267    [C, N, H, E, G, K, L, F]
        //P2: Route: fit: 2258    [L, C, K, H, N, E, G, F]
        p1.addStop(Vertex.findByNickname(g.vertices, "C").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "N").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "H").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "E").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "G").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "K").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "L").orElseThrow());
        p1.addStop(Vertex.findByNickname(g.vertices, "F").orElseThrow());
        var p2 = new Route(rnd.nextLong());
        p2.addStop(Vertex.findByNickname(g.vertices, "L").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "C").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "K").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "H").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "N").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "E").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "G").orElseThrow());
        p2.addStop(Vertex.findByNickname(g.vertices, "F").orElseThrow());
        var offspring = p1.pmx(p2);
        return g;
    }
}

package br.univali.graph.model;

public class Arc {
    private static int ID = 0;
    protected final int id;
    protected final Vertex end;
    protected int color = 0;
    protected int cost;

    public Arc(Vertex end, int cost) {
        this.end = end;
        this.cost = cost;
        this.id = ID++;
    }

    // Manual Arc ID for non-directed graphs
    protected Arc(int id, Vertex end, int cost) {
        this.id = id;
        this.cost = cost;
        this.end = end;
    }

    @Override
    public String toString() {
        return "A" + id +
                "{ end=" + end.toString() +
                ", cost=" + cost +
                " }";
    }

    public int getId() {
        return this.id;
    }

    public int getEndId() {
        return this.end.id;
    }

    public int getCost() {
        return this.cost;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Arc a) {
            return a.id == this.id;
        }
        return false;
    }

    public String toShortString() {
        return "A" + id;
    }
}
